var url = location.href;
var issue = url.split('/').pop();

$(function(){
	if(issue.match(/[A-Z]*-[0-9]*/i)){

		injectTimer();
		initLocalStorage();
		initListeners(issue);
		startTimer(issue);
	}
});

function injectTimer(issue) {
	var template = '<li class="item full-width"> \
			            <div class="wrap" id="wrap-timer"> \
			                <strong class="name">Timer:</strong> \
			            	<span class="labels"><img id="timer-start" src="{{icon_play}}"> <img id="timer-pause" src="{{icon_pause}}" style="display:none;"> <input type="text" readonly="readonly" id="timer-log-work" value="0w 0d 0h 0m 0s" /><img id="timer-reset" src="{{icon_reset}}"> <img id="log-add" src="{{icon_add}}" style="display: none;"></span> \
			            </div> \
			        </li>';
	var view = {
		icon_play: chrome.extension.getURL("images/icons/control_play.png"),
		icon_reset: chrome.extension.getURL("images/icons/control_repeat.png"),
		icon_pause: chrome.extension.getURL("images/icons/control_pause.png"),
		icon_add: chrome.extension.getURL("images/icons/time_add.png")
	}

	$('#issuedetails').append(Mustache.render(template, view));
}

function initLocalStorage() {
	if($.jStorage.get('timers') === null) {
		$.jStorage.set('timers', Object.extended());
	}
}

function initListeners(issue) {

	$('#log-add').on('click', function(event){
		timers = $.jStorage.get('timers');
		if(Object.has(timers, issue)) {
			if(timers[issue].status == 'running') {
				$('#timer-pause').trigger('click');
			}
		}
		
		var link = document.getElementById('log-work');		
		link.click();
	});

	$('#log-work-time-logged').waitUntilExists(function(){
		$this = $(this);
		$this.val($('#timer-log-work').val());
	});

	$('#timer-start').on('click', function(event){
		$this = $(this);
		$this.hide();
		$('#timer-pause').show();
		$('#log-add').show();

		timer = setInterval(updateTimer, 1000);

		timers = $.jStorage.get('timers');

		if(Object.has(timers, issue)) {
			if (timers[issue].status == 'paused') {
				timers[issue].timestamp = Date.now();
				timers[issue].status = 'running';
			}
		} else {
			timers[issue] = {
				timestamp: Date.now(),
				offset: 0,
				status: 'running'
			};	
		}

		$.jStorage.set('timers', timers);
	});

	$('#timer-pause').on('click', function(event){
		$this = $(this);
		$this.hide();
		$('#timer-start').show();

		if(typeof timer !== 'undefined') {
			clearInterval(timer);
		}		
		
		timers = $.jStorage.get('timers');
		var now = Date.now();
		var time = (now - parseInt(timers[issue].timestamp));
	    var elapsed = Math.round(Math.floor(time / 100) / 10);

	    timers[issue].offset += elapsed;
	    timers[issue].status = 'paused';
	    $.jStorage.set('timers', timers);
	});

	$('#timer-reset').on('click', function(event){
		$('#timer-pause').hide();
		$('#log-add').hide();
		$('#timer-start').show();
		$('#timer-log-work').val('0w 0d 0h 0m 0s');

		if(typeof timer !== 'undefined') {
			clearInterval(timer);
		}

		timers = $.jStorage.get('timers');
		delete timers[issue];

		$.jStorage.set('timers', timers);

		console.log($.jStorage.get('timers'));
	});
}

function startTimer(issue) {
	var timers = $.jStorage.get('timers');
	console.log(timers);
	if(Object.has(timers, issue)) {
		if(timers[issue].status == 'running') {
			$('#timer-start').trigger('click');
		}

		if(timers[issue].status == 'paused') {
			$('#timer-log-work').val(formatTime(timers[issue].offset));
			$('#log-add').show();
		}
	}
}

function updateTimer() {

	var now = Date.now();
	var $timer = $('#timer-log-work');

	timers = $.jStorage.get('timers');
	var time = (now - parseInt(timers[issue].timestamp));
    var elapsed = Math.round(Math.floor(time / 100) / 10);
    elapsed += timers[issue].offset;
    $timer.val(formatTime(elapsed));
}

function formatTime(seconds) {

    var weekFactor = 144000; //5 Days
    var dayFactor = 28800;	 //8 Hours
    var hourFactor = 3600;	 //60 Minutes
    var minFactor = 60;		 //60 Seconds
    var secFactor = 1;		 //1 Seconds

    var view = {
        weeks : 0,
        days : 0,
        hours : 0,
        mins: 0,
        secs : 0
    };

    view.weeks = Math.floor(seconds / weekFactor);
    seconds -= view.weeks * weekFactor;

    view.days = Math.floor(seconds / dayFactor);
    seconds -= view.days * dayFactor;

    view.hours = Math.floor(seconds / hourFactor);
    seconds -= view.hours * hourFactor;

    view.mins = Math.floor(seconds / minFactor);
    seconds -= view.mins * minFactor;

    view.secs = seconds;
    return Mustache.render('{{weeks}}w {{days}}d {{hours}}h {{mins}}m {{secs}}s', view);
}